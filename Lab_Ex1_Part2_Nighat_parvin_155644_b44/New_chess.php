<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP</title>
    <style>
        .white, .black {
            width: 30px;
            height: 30px;
            display: block;
            float: left;
        }
        .white {
            background: #FFFFFF;
        }
        .black {
            background: #000000;
        }
        .board {
            width: 240px;
            height:240px;
            border: 1px solid black;
        }


    </style>
</head>
<body>


<div class="board">
    <?php
    for($row=0;$row<8;$row++){
        for($column=0;$column<8;$column++){
            $color = ($row+$column)%2 ? 'black' : 'white';
            echo '<div class="'.$color.'">&nbsp;</div>';
        }
    }
    ?>
</div>

</body>
</html>