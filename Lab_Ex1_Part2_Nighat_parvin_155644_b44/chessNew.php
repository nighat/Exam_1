<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PHP</title>
    <style>
        .white, .black {
            width: 30px;
            height: 30px;
            display: block;
            float: left;
        }
        .white {
            background: #FFFFFF;
        }
        .black {
            background: #000000;
        }
        .dd {
            width: 240px;
            height: 240px;
            border: 1px solid black;
        }


    </style>
</head>
<body>


<div>
    <?php
    echo "<form action='' method='post'>
    <input type='text' name='num' placeholder='Enter a number'>
    <input type='submit' >
</form>";

    $num = $_POST['num'];


    for($row=0;$row<$num;$row++){
        for($column=0;$column<$num;$column++){
            $color = ($row+$column)%2 ? 'black' : 'white';
            echo '<div class="'.$color.'">&nbsp;</div>';

        }
    }
    ?>
</div>